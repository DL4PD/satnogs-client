Welcome to SatNOGS Client's documentation!
==========================================

.. toctree::
   :maxdepth: 4

   architecture
   satnogsclient
   installation
